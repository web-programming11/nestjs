import { MinLength, Min } from 'class-validator';
export class CreateProductDto {
  @MinLength(8)
  name: string;
  @Min(0)
  price: number;
}
